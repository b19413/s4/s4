//! [A] 

SELECT * FROM artists WHERE name LIKE ("%d%");

//! [B]

SELECT song_name FROM songs WHERE length < 230;

//! [C]


SELECT album_title, song_name, length FROM albums 
    JOIN songs ON albums.id = songs.album_id;

//! [D]

SELECT * FROM artists 
    JOIN albums ON artists.id = albums.artist_id 
    WHERE album_title LIKE "%a%";


//! [E]

SELECT * FROM albums WHERE id IN (1,2,3,4) ORDER BY album_title ASC;


//! [F] 

NOTE : Since we cannot sort both album_title and song_name at the same time we have to create a query where we can sort album_title and song_name seperately.

SELECT * FROM  albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC;

SELECT * FROM  albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY song_name DESC;
   